DESCRIPTION:
------------
This module provides a clerarer subject to test newsletters send by the simplenews module.


REQUIREMENTS:
-------------
simplenews_test_subject.module requires the simplenews module.


INSTALLATION:
-------------
1. Place the entire simplenews_test_subject directory into your Drupal modules/
   directory.

2. Enable the simplenews_test_subject module by navigating to:

     administer > modules


Features:
---------
* test newsletter submission will have its subject line preceeded by the text "TEST - "


Author:
-------
Peter Carrero
peter.carrero@gmail.com
http://drupal.org/user/114207
http://www.petercarrero.com